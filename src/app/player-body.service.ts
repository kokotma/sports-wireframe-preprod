import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class PlayerBodyService {
  marker: number[][] = [];
  bodyparts: any = [
    {name: 'Head', value: [19.1, 5.1]},
    {name: 'Neck', value: [19.1, 14.8]},
    {name: 'Shoulder', value: [6.2, 19.6]}
  ];
  bodyPartDetails: any = [
    {name: 'Head', value: [{name: 'Concussion', id: 1}, {name: 'Headaches', id: 2}, {name: 'Cannot Play', id: 3}]},
    {name: 'Neck', value: [{name: 'Tense', id: 4}, {name: 'Sore', id: 5}, {name: 'Cannot Play', id: 6}]},
    {name: 'Shoulder', value: [{name: 'Broken', id: 7}, {name: 'Recovering', id: 8}, {name: 'Cannot Play', id: 9}]}
  ];
  additionalPartDetails: any = [
    {id: 1, value: [{info: 'Concussion due to headbutting things'}]},
    {id: 2, value: [{info: 'Headaches due to the concussion'}]},
    {id: 3, value: [{info: 'Cannot play because he has a headache due to the concussion'}]},
    {id: 4, value: [{info: 'Tense neck due to not stretching correctly'}]},
    {id: 5, value: [{info: 'Sore due to over working non-stretched neck'}]},
    {id: 6, value: [{info: 'Cannot play because he\'s silly'}]},
    {id: 7, value: [{info: 'Broken Shoulder due to running into wall'}]},
    {id: 8, value: [{info: 'Recovering now'}]},
    {id: 9, value: [{info: 'Cannot play due to broken shoulder'}]}
  ];
  constructor() { }
  getMarkers(): any  {
    for (const items of this.bodyparts) {
        this.marker.push(items.value);
    }
    return this.marker
  }
  getBodyparts( part: number[] ): any {
    for (const entry of this.bodyparts) {
      if ( entry.value ===  part) {
        return entry.name;
      }
    }
  }
  getBodypartDetails( part: string ): any {
    for (const entry of this.bodyPartDetails) {
      if ( entry.name === part) {
        return entry.value;
      }
    }
  }
  getAddBodypartDetails( part: number ): any {
    for (const entry of this.additionalPartDetails) {
      if ( entry.id === part) {
        return entry.value;
      }
    }
  }
}
