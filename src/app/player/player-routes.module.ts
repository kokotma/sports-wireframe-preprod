import { PlayerDataComponent } from './../player-data/player-data.component';
import { PlayerProfileComponent } from './../player-profile/player-profile.component';
import { PlayerPhysioComponent } from './../player-physio/player-physio.component';
import { PlayerDetailComponent } from './../player-detail/player-detail.component';
import { PlayerComponent } from './player.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

const playerRoutes: Routes = [
  { path: 'player', component: PlayerComponent },
  {
    path: 'player/:player_id',
    component: PlayerDetailComponent,
    children: [
      {
        path: 'physio',
        component: PlayerPhysioComponent
      },
      {
        path: 'profile',
        component: PlayerProfileComponent
      },
      {
        path: 'data',
        component: PlayerDataComponent
      },
      {
        path: '',
        redirectTo: 'profile',
        pathMatch: 'full'
      }
    ]

  }
]


@NgModule({
  imports: [
    RouterModule.forChild(playerRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class PlayerRoutesModule { }
