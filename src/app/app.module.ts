import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MdButtonModule,
  MdCheckboxModule,
  MdSidenavModule,
  MdListModule,
  MdIconModule,
  MdToolbarModule,
  MdCardModule,
  MdAutocompleteModule,
  MdInputModule,
  MdProgressBarModule,
  MdTabsModule,
  MdGridListModule,
  MdTableModule,
  MdDialogModule,
  MdDialog } from '@angular/material';
import { HttpModule } from '@angular/http';
import { PlayerRoutesModule } from './player/player-routes.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CdkTableModule } from '@angular/cdk';

import 'hammerjs';

import { AppComponent } from './app.component';
import { HomeComponent, AlertDialogComponent } from './home/home.component';
import { PlayerComponent } from './player/player.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DataComponent } from './data/data.component';
import { AdminComponent } from './admin/admin.component';
import { PlayerGridComponent } from './player-grid/player-grid.component';
import { ChartsModule } from 'ng2-charts';

import { PlayerService } from './player.service';
import { DashboardService } from './dashboard.service';
import { AlertsService } from './alerts.service';
import { TableService } from './table.service';

import { FilterPipe } from './filter.pipe';
import { PlayerDetailComponent } from './player-detail/player-detail.component';
import { PhysioComponent } from './physio/physio.component';
import { PlayerProfileComponent } from './player-profile/player-profile.component';
import { PlayerPhysioComponent } from './player-physio/player-physio.component';
import { PlayerDataComponent } from './player-data/player-data.component';
import { SafePipe } from './safe.pipe';
import { LoginComponent } from './login/login.component';
import { PlayerBodyComponent } from './player-body/player-body.component';
import { ImgMapComponent } from 'ng2-img-map';

const appRoutes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'physio', component: PhysioComponent },
  { path: 'data', component: DataComponent },
  { path: 'admin', component: AdminComponent },
  { path: 'login', component: LoginComponent },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PlayerComponent,
    DashboardComponent,
    DataComponent,
    AdminComponent,
    PlayerGridComponent,
    FilterPipe,
    PlayerDetailComponent,
    PhysioComponent,
    PlayerProfileComponent,
    PlayerPhysioComponent,
    PlayerDataComponent,
    SafePipe,
    LoginComponent,
    AlertDialogComponent,
    PlayerBodyComponent,
    ImgMapComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    PlayerRoutesModule,
    RouterModule.forRoot(
      appRoutes
    ),
    FormsModule,
    MdButtonModule, MdCheckboxModule, MdSidenavModule, MdListModule,
    MdIconModule, MdToolbarModule, MdCardModule, MdAutocompleteModule,
    MdInputModule, MdProgressBarModule, MdTabsModule, MdGridListModule,
    MdTableModule, CdkTableModule, MdDialogModule,
    ChartsModule,
    FlexLayoutModule
  ],
  providers: [PlayerService, DashboardService, TableService, MdDialog, AlertsService],
  entryComponents: [AlertDialogComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
