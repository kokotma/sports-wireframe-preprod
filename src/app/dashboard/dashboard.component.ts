import { DashboardService } from './../dashboard.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  categories: Array<any>;
  tableauUrl: string;

  constructor(private service: DashboardService) { }

  ngOnInit() {
    this.service.getCategories()
                .subscribe((categories) => this.categories = categories);

  }

  onClick(uri: string) {
    this.tableauUrl = uri;
  }

}
