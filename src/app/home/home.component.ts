import { AlertsService } from './../alerts.service';
import { Component, OnInit, Inject } from '@angular/core';
import { MD_DIALOG_DATA, MdDialog } from '@angular/material';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  userName = 'User';
  alerts = [
    {header: '5', message: 'Data Uploads', id: 1 },
    {header: '6', message: 'Player Alerts', id: 2 },
    {header: '8', message: 'Events', id: 3 },
    {header: '1', message: 'Alert 4', id: 4}
  ];

  constructor(public dialog: MdDialog) { }

  ngOnInit() {
  }

  onClick(alertId: number) {
    let dialogRef = this.dialog.open(AlertDialogComponent, {
      data: alertId,
    });
  }
}

@Component({
  selector: 'app-alert-dialog',
  template: 'passed in {{ alert | json }}',
})

export class AlertDialogComponent implements OnInit {

  alert: any;

  constructor(@Inject(MD_DIALOG_DATA) public data: any, public alertService: AlertsService) { }

  ngOnInit() {
    this.alert = this.alertService.getAlert(this.data);
  }

}
