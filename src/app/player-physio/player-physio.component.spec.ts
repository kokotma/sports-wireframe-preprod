import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerPhysioComponent } from './player-physio.component';

describe('PlayerPhysioComponent', () => {
  let component: PlayerPhysioComponent;
  let fixture: ComponentFixture<PlayerPhysioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerPhysioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerPhysioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
