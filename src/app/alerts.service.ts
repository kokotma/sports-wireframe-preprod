import { Injectable } from '@angular/core';

@Injectable()
export class AlertsService {

  constructor() { }

  getAlert(alertId: number): any {
    return {id: alertId, message: 'This is an alert'};
  }


}
