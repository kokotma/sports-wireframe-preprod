import { Component, OnInit, ViewChild } from '@angular/core';
import { ImgMapComponent } from 'ng2-img-map';
import {PlayerBodyService} from '../player-body.service';

@Component({
  selector: 'app-player-body',
  templateUrl: './player-body.component.html',
  styleUrls: ['./player-body.component.css'],
  providers: [PlayerBodyService]
})
export class PlayerBodyComponent implements OnInit {
  currentMarker: string;
  currentPlayerDetail: string;
  playerDetail: string;
  playerAddDetail: string;
  markers = this._playerBody.getMarkers();
  @ViewChild('imgMap')
  imgMap: ImgMapComponent;
  // When Marker Changes
  onChange(marker: number[]) {
    console.log('Markers', marker);
    this.currentMarker = this._playerBody.getBodyparts(marker)
    this.playerDetail = this._playerBody.getBodypartDetails(this.currentMarker)
    this.currentPlayerDetail = '';
  }
  // When marker is selected
  selectMarker(index: number) {
    this.imgMap.markerActive = index;
    this.imgMap.draw();
  }
  constructor(private _playerBody: PlayerBodyService) { }
  ngOnInit() {
  }
  onDetailOneClick(id: number, name: string) {
    this.playerAddDetail = this._playerBody.getAddBodypartDetails(id);
    this.currentPlayerDetail = name;
    console.log(this.playerAddDetail);
  }

}


