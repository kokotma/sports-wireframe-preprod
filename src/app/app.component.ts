import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { MdSidenav } from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  isOpened: boolean = false;
  private minNavWidth: number = 1024;

  @ViewChild(MdSidenav)
  sidenav: MdSidenav;

  ngOnInit() {
    this.onResize(window.innerWidth);
  }

  @HostListener('window:resize', ['$event.target.innerWidth'])
  onResize(width) {
    this.isOpened = width > this.minNavWidth;
  }

}
