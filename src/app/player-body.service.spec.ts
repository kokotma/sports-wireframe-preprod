import { TestBed, inject } from '@angular/core/testing';

import { PlayerBodyService } from './player-body.service';

describe('PlayerBodyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PlayerBodyService]
    });
  });

  it('should be created', inject([PlayerBodyService], (service: PlayerBodyService) => {
    expect(service).toBeTruthy();
  }));
});
