import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Http, Response, Headers, RequestOptions, RequestOptionsArgs } from '@angular/http';

import 'rxjs/Rx';

@Injectable()
export class DashboardService {

  constructor(private http: Http) { }

  getCategories(): Observable<any[]> {
    let urlFormatted = 'assets/json/dash-category.json';
    return this.http.get(urlFormatted)
      .map(this.extractData)
      .catch(this.handleError);
  }

  private extractData(res: Response) {
    let body = res.json();
    return body || {};
  }

  private handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }


}
