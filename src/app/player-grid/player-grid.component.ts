import { PlayerService } from './../player.service';
import { Component, OnInit, Output, EventEmitter} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-player-grid',
  templateUrl: './player-grid.component.html',
  styleUrls: ['./player-grid.component.css']
})
export class PlayerGridComponent implements OnInit {

  @Output() onLoaded = new EventEmitter<boolean>();
  players: any[];
  input: string;
  ready: boolean = false;

  constructor(private service: PlayerService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
      this.service.getPlayers()
      .subscribe((players) => this.players = players
                , (err) => 0
                , () => {this.onLoaded.emit(true); this.ready = true; });
  }

  onClick(player_id: number) {
    this.router.navigate(['/player', player_id]);
  }

  onChange(e) {
    console.log(event);
  }

}
