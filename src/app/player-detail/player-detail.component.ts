import { Component, OnInit, HostBinding } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs/add/operator/switchMap';

import { PlayerService } from './../player.service';


@Component({
  selector: 'app-player-detail',
  templateUrl: './player-detail.component.html',
  styleUrls: ['./player-detail.component.css']
})
export class PlayerDetailComponent implements OnInit {

  selectedPlayer: any;
  navLinks = [ { label: 'Profile', link: 'profile'},
               { label: 'Physio', link: 'physio' },
               { label: 'Data', link: 'data' }];

  constructor(private service: PlayerService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.route.params
      .switchMap((params: Params) => this.service.getPlayer(+params['player_id']))
      .subscribe((player: any) => this.selectedPlayer = player, (err) => true, () => console.log(this.selectedPlayer) );
  }

}
