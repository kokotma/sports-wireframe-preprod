import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Http, Response, Headers, RequestOptions, RequestOptionsArgs } from '@angular/http';

import 'rxjs/Rx';



export class Player {
  constructor(id?: number, name?: string, number?: string ) {}
}

export class Metric {
  constructor(labels: string[], data: number[] ) {}
}

@Injectable()
export class PlayerService {

  constructor(private http: Http) { }

  getPlayer(player_id: number): Observable<any> {

    let urlFormatted = `assets/json/players.json`;

    console.log(player_id);
    console.log(typeof(player_id));

    return this.http.get(urlFormatted)
      .flatMap((response) => response.json())
      .filter((person: any) => { return person.id === player_id})
      .map((person) => {console.log(person); return person; })
      .catch(this.handleError);
  }

  getMetrics(player_id: number): Observable<Metric> {

    let metric = new Metric(
      ['a', 'b', 'c', 'd'],
      [1, 2, 3, 4, 5, 6]
    );

    return Observable.of(metric);

  }

  getPlayers(): Observable<any[]> {
    let urlFormatted = 'assets/json/players.json';
    return this.http.get(urlFormatted)
      .map(this.extractData)
      .catch(this.handleError);
  }

  search(term: string): Observable<any[]> {

    let urlFormatted = `assets/json/players.json`;

    if (term.length === 0) {
      console.log('all players');
      return this.getPlayers();
    }

    console.log('search term from service');
    return this.http.get(urlFormatted)
      .map((response) => response.json())
      .filter((person: any) => person.name.indexOf(term) >= 0);
  }

  private extractData(res: Response) {
    let body = res.json();
    return body || {};
  }

  private handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
