import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(items: any[], args: any): any {
        if (args !== undefined) {
          return items.filter(item => item.name.toLowerCase().indexOf(args.toLowerCase()) > -1);
        }
        return items;
    }

}