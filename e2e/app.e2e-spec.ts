import { CarltonWireframeProjectPage } from './app.po';

describe('carlton-wireframe-project App', () => {
  let page: CarltonWireframeProjectPage;

  beforeEach(() => {
    page = new CarltonWireframeProjectPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
